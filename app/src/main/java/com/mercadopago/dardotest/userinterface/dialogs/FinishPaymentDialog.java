package com.mercadopago.dardotest.userinterface.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.models.Transaction;

public class FinishPaymentDialog extends DialogFragment {
    private static final String KEY = "transaction";

    public static FinishPaymentDialog newInstance(Transaction transaction) {
        FinishPaymentDialog dialog = new FinishPaymentDialog();
        Bundle args = new Bundle();
        args.putParcelable(KEY, transaction);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = createCustomView((Transaction) getArguments().getParcelable(KEY));

        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setView(view)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, onOKPressedListener)
                .create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    private View createCustomView(Transaction transaction) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.finish_payment_dialog, null, false);
        TextView amountTextView = view.findViewById(R.id.amountTextView);
        TextView paymentMethosTextView = view.findViewById(R.id.paymentMethosTextView);
        TextView cardIssuerTextView = view.findViewById(R.id.cardIssuerTextView);
        TextView installmentsTextView = view.findViewById(R.id.installmentsTextView);

        amountTextView.setText(transaction.getAmount().toString());
        paymentMethosTextView.setText(transaction.getPaymentMethod().getName());
        cardIssuerTextView.setText(transaction.getCardIssuer().getName());
        installmentsTextView.setText(transaction.getInstallmentItem().getRecommendedMessage());

        return view;
    }

    private DialogInterface.OnClickListener onOKPressedListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dismiss();
        }
    };
}

