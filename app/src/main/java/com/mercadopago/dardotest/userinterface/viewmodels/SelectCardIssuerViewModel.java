package com.mercadopago.dardotest.userinterface.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.flowmanagers.PaymentManager;
import com.mercadopago.dardotest.models.CardIssuer;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.util.ArrayList;
import java.util.List;

public class SelectCardIssuerViewModel extends AndroidViewModel {

    private Transaction transaction;
    private DisposableRequest ongoingRequest;
    private final PaymentManager paymentManager = PaymentManager.getInstance();
    private final MutableLiveData<List<CardIssuer>> cardIssuerListObservable = new MutableLiveData<>();
    private final MutableLiveData<String> validateModelResponseObservable = new MutableLiveData<>();

    public MutableLiveData<String> getValidateModelResponseObservable() {
        return validateModelResponseObservable;
    }

    public LiveData<List<CardIssuer>> getCardIssuerListObservable() {
        return cardIssuerListObservable;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public SelectCardIssuerViewModel(@NonNull Application application) {
        super(application);
    }

    public void dispose() {
        if (ongoingRequest != null) {
            ongoingRequest.dispose();
            ongoingRequest = null;
        }
    }

    public void retrieveModel(boolean forceRefresh) {
        ongoingRequest = paymentManager.getCardIssuers(transaction.getPaymentMethod(), onCardIssuersCompleted, forceRefresh);
    }

    private OnListRequestCompleted<CardIssuer> onCardIssuersCompleted = new OnListRequestCompleted<CardIssuer>() {
        @Override
        public void onSuccess(ArrayList<CardIssuer> cardIssuers) {
            ongoingRequest = null;
            cardIssuerListObservable.setValue(cardIssuers);
        }

        @Override
        public void onError(Throwable throwable) {
            ongoingRequest = null;
            cardIssuerListObservable.setValue(null);
        }
    };

    public void selectCardIssuer(CardIssuer cardIssuer) {
        transaction.setCardIssuer(cardIssuer);
        transaction.setInstallmentItem(null);
    }

    public void validateModel() {
        validateModelResponseObservable.setValue(
                transaction.getCardIssuer() == null ? getApplication().getString(R.string.error_card_issuer) : null
        );
    }}
