package com.mercadopago.dardotest.userinterface.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.helpers.ImageDownloader;
import com.mercadopago.dardotest.models.PaymentMethod;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.PaymentMethodsViewHolder>{

    private List<PaymentMethod> paymentMethods = new ArrayList<>();
    private OnPaymentMethodClickedListener onPaymentMethodClickedListener;
    private int selectedPosition = -1;

    public PaymentMethodsAdapter(OnPaymentMethodClickedListener onPaymentMethodClickedListener) {
        this.onPaymentMethodClickedListener = onPaymentMethodClickedListener;
    }

    public void updatePaymentMethods(List<PaymentMethod> paymentMethods) {
        selectedPosition = -1;
        this.paymentMethods = paymentMethods;
        notifyDataSetChanged();
    }

    public void selectPaymentMethodById(String id) {
        selectedPosition = -1;
        for(int i=0; i<paymentMethods.size(); i++) {
            if(paymentMethods.get(i).getId().equals(id)) {
                selectedPosition = i;
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public PaymentMethodsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_method_item, parent, false);
        return new PaymentMethodsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PaymentMethodsViewHolder holder, int position) {
        final PaymentMethod paymentMethod = paymentMethods.get(position);
        holder.nameTextView.setText(paymentMethod.getName());
        ImageDownloader.getInstance().retrieveImage(paymentMethod.getThumbnail(), holder.avatarImageView, R.mipmap.default_card);
        holder.itemView.setBackgroundColor(Color.parseColor(selectedPosition == position ? "#ababae" : "#ffffff"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = holder.getAdapterPosition();
                if(onPaymentMethodClickedListener != null) {
                    onPaymentMethodClickedListener.onPaymentMethodClicked(paymentMethod);
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentMethods.size();
    }

    public class PaymentMethodsViewHolder extends RecyclerView.ViewHolder {
        private ImageView avatarImageView;
        public TextView nameTextView;

        public PaymentMethodsViewHolder(View view) {
            super(view);
            avatarImageView = view.findViewById(R.id.avatarImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
        }
    }

    public interface OnPaymentMethodClickedListener {
        void onPaymentMethodClicked(PaymentMethod paymentMethod);
    }
}
