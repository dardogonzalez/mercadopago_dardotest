package com.mercadopago.dardotest.userinterface.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.flowmanagers.PaymentManager;
import com.mercadopago.dardotest.models.Installment;
import com.mercadopago.dardotest.models.InstallmentItem;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.util.ArrayList;
import java.util.List;

public class SelectInstallmentsViewModel extends AndroidViewModel {

    private Transaction transaction;
    private DisposableRequest ongoingRequest;
    private final PaymentManager paymentManager = PaymentManager.getInstance();
    private final MutableLiveData<List<InstallmentItem>> installmentItemsListObservable = new MutableLiveData<>();
    private final MutableLiveData<String> validateModelResponseObservable = new MutableLiveData<>();

    public MutableLiveData<String> getValidateModelResponseObservable() {
        return validateModelResponseObservable;
    }

    public LiveData<List<InstallmentItem>> getInstallmentItemsListObservable() {
        return installmentItemsListObservable;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public SelectInstallmentsViewModel(@NonNull Application application) {
        super(application);
    }

    public void dispose() {
        if (ongoingRequest != null) {
            ongoingRequest.dispose();
            ongoingRequest = null;
        }
    }

    public void retrieveModel(boolean forceRefresh) {
        ongoingRequest = paymentManager.getInstallments(transaction.getPaymentMethod(), transaction.getCardIssuer(), transaction.getAmount(), onInstallmentsCompleted, false);
    }

    private OnListRequestCompleted<InstallmentItem> onInstallmentsCompleted = new OnListRequestCompleted<InstallmentItem>() {
        @Override
        public void onSuccess(ArrayList<InstallmentItem> installmentItems) {
            ongoingRequest = null;
            installmentItemsListObservable.setValue(installmentItems);
        }

        @Override
        public void onError(Throwable throwable) {
            ongoingRequest = null;
            installmentItemsListObservable.setValue(null);
        }
    };

    public void selectInstallmentItem(InstallmentItem installmentItem) {
        transaction.setInstallmentItem(installmentItem);
    }

    public void validateModel() {
        validateModelResponseObservable.setValue(
                transaction.getInstallmentItem() == null ? getApplication().getString(R.string.error_installment) : null
        );
    }
}
