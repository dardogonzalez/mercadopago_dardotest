package com.mercadopago.dardotest.userinterface.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.databinding.ActivitySelectPaymentMethodBinding;
import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.userinterface.adapters.PaymentMethodsAdapter;
import com.mercadopago.dardotest.userinterface.viewmodels.SelectPaymentMethodViewModel;

import java.util.List;

public class SelectPaymentMethodActivity extends AppCompatActivity {

    private PaymentMethodsAdapter paymentMethodsAdapter;
    private SelectPaymentMethodViewModel viewModel;
    private ActivitySelectPaymentMethodBinding binding;

    @InjectExtra Transaction transaction;

    public static Intent createStartIntent(Context context, Transaction transaction) {
        return Henson.with(context).gotoSelectPaymentMethodActivity().transaction(transaction).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dart.inject(this);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_select_payment_method, null, false);
        setContentView(binding.getRoot());

        setupActionBar();
        setupView();
        attachViewModel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.dispose();
    }

    private void setupActionBar() {
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.select_payment_method_activity_action_bar_title);
        binding.toolbar.setNavigationOnClickListener(backNavigationOnClickListener);
    }

    private void setupView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        paymentMethodsAdapter = new PaymentMethodsAdapter(onPaymentMethodClickedListener);
        binding.recyclerView.setAdapter(paymentMethodsAdapter);
        binding.swipeRefreshLayout.setOnRefreshListener(swipeRefreshLayoutOnRefreshListener);
    }

    private void attachViewModel() {
        viewModel = ViewModelProviders.of(this).get(SelectPaymentMethodViewModel.class);
        viewModel.setTransaction(transaction);
        viewModel.getPaymentMethodListObservable().observe(this, paymentMethodListObserver);
        viewModel.getValidateModelResponseObservable().observe(this, modelValidationResponseObserver);
        binding.setViewModel(viewModel);
        binding.swipeRefreshLayout.setRefreshing(true);
        viewModel.retrieveModel(false);
    }

    private SwipeRefreshLayout.OnRefreshListener swipeRefreshLayoutOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            viewModel.retrieveModel(true);
        }
    };

    private PaymentMethodsAdapter.OnPaymentMethodClickedListener onPaymentMethodClickedListener = new PaymentMethodsAdapter.OnPaymentMethodClickedListener() {
        @Override
        public void onPaymentMethodClicked(PaymentMethod paymentMethod) {
            viewModel.selectPaymentMethod(paymentMethod);
        }
    };

    private Observer<String> modelValidationResponseObserver = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String error) {
            if(error != null) {
                Toast.makeText(SelectPaymentMethodActivity.this, error, Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = SelectCardIssuerActivity.createStartIntent(SelectPaymentMethodActivity.this, transaction);
                startActivity(intent);
                finish();
            }
        }
    };

    private Observer<List<PaymentMethod>> paymentMethodListObserver = new Observer<List<PaymentMethod>>() {
        @Override
        public void onChanged(@Nullable List<PaymentMethod> paymentMethods) {
            binding.swipeRefreshLayout.setRefreshing(false);
            if(paymentMethods == null) {
                binding.setModelLoadingError(true);
                return;
            }
            binding.setModelLoadingError(false);
            paymentMethodsAdapter.updatePaymentMethods(paymentMethods);
            if (transaction.getPaymentMethod() != null) {
                paymentMethodsAdapter.selectPaymentMethodById(transaction.getPaymentMethod().getId());
            }
        }
    };

    private View.OnClickListener backNavigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = MainActivity.createStartIntent(SelectPaymentMethodActivity.this);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = EnterAmountActivity.createStartIntent(this, transaction);
        startActivity(intent);
        finish();
    }

}
