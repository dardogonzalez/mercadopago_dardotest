package com.mercadopago.dardotest.userinterface.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.databinding.ActivitySelectInstallmentsBinding;
import com.mercadopago.dardotest.models.InstallmentItem;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.userinterface.adapters.InstallmentItemsAdapter;
import com.mercadopago.dardotest.userinterface.dialogs.GenericOneButtonDialog;
import com.mercadopago.dardotest.userinterface.viewmodels.SelectInstallmentsViewModel;

import java.util.List;

public class SelectInstallmentsActivity extends AppCompatActivity implements GenericOneButtonDialog.OnGenericOneButtonDialogClosedListener {

    private InstallmentItemsAdapter installmentsItemsAdapter;
    private SelectInstallmentsViewModel viewModel;
    private ActivitySelectInstallmentsBinding binding;

    @InjectExtra Transaction transaction;

    public static Intent createStartIntent(Context context, Transaction transaction) {
        return Henson.with(context).gotoSelectInstallmentsActivity().transaction(transaction).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dart.inject(this);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_select_installments, null, false);
        setContentView(binding.getRoot());

        setupActionBar();
        setupView();
        attachViewModel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.dispose();
    }

    private void setupActionBar() {
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.select_installments_activity_action_bar_title);
        binding.toolbar.setNavigationOnClickListener(backNavigationOnClickListener);
    }

    private void setupView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        installmentsItemsAdapter = new InstallmentItemsAdapter(onInstallmentItemClickedListener);
        binding.recyclerView.setAdapter(installmentsItemsAdapter);
        binding.swipeRefreshLayout.setOnRefreshListener(swipeRefreshLayoutOnRefreshListener);
    }

    private void attachViewModel() {
        viewModel = ViewModelProviders.of(this).get(SelectInstallmentsViewModel.class);
        viewModel.setTransaction(transaction);
        viewModel.getInstallmentItemsListObservable().observe(this, installmentItemsListObserver);
        viewModel.getValidateModelResponseObservable().observe(this, modelValidationResponseObserver);
        binding.setViewModel(viewModel);
        binding.swipeRefreshLayout.setRefreshing(true);
        viewModel.retrieveModel(false);
    }

    private SwipeRefreshLayout.OnRefreshListener swipeRefreshLayoutOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            viewModel.retrieveModel(true);
        }
    };

    private InstallmentItemsAdapter.OnInstallmentItemClickedListener onInstallmentItemClickedListener = new InstallmentItemsAdapter.OnInstallmentItemClickedListener() {
        @Override
        public void onInstallmentItemClicked(InstallmentItem installmentItem) {
            viewModel.selectInstallmentItem(installmentItem);
        }
    };

    private Observer<String> modelValidationResponseObserver = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String error) {
            if(error != null) {
                Toast.makeText(SelectInstallmentsActivity.this, error, Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = MainActivity.createStartIntent(SelectInstallmentsActivity.this, transaction);
                startActivity(intent);
                finish();
            }
        }
    };

    private Observer<List<InstallmentItem>> installmentItemsListObserver = new Observer<List<InstallmentItem>>() {
        @Override
        public void onChanged(@Nullable List<InstallmentItem> installmentItems) {
            binding.swipeRefreshLayout.setRefreshing(false);
            processInstallments(installmentItems);
        }
    };

    private void processInstallments(List<InstallmentItem> installmentItems) {
        if(installmentItems == null) {
            binding.setModelLoadingError(true);
            return;
        }
        if(installmentItems.isEmpty()) {
            showInstallmentsNotFound();
            return;
        }
        binding.setModelLoadingError(false);
        installmentsItemsAdapter.updateInstallmentItem(installmentItems);
        if (transaction.getInstallmentItem() != null) {
            installmentsItemsAdapter.selectInstallmentItem(transaction.getInstallmentItem().getQuantities());
        }
    }

    private void showInstallmentsNotFound() {
        GenericOneButtonDialog dialog = GenericOneButtonDialog.newInstance(R.string.installments_not_found);
        dialog.show(getSupportFragmentManager(), "GenericOneButtonDialogTag");
    }

    @Override
    public void onGenericOneButtonDialogClose(String tag) {
        onBackPressed();
    }

    private View.OnClickListener backNavigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = MainActivity.createStartIntent(SelectInstallmentsActivity.this);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = SelectCardIssuerActivity.createStartIntent(this, transaction);
        startActivity(intent);
        finish();
    }

}

