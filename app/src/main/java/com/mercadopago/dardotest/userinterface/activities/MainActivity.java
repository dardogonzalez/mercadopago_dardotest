package com.mercadopago.dardotest.userinterface.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.flowmanagers.PaymentManager;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.userinterface.dialogs.FinishPaymentDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private ActionBar actionBar;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @InjectExtra @Nullable Transaction transaction;

    public static Intent createStartIntent(Context context) {
        return Henson.with(context).gotoMainActivity().build();
    }

    public static Intent createStartIntent(Context context, Transaction transaction) {
        return Henson.with(context).gotoMainActivity().transaction(transaction).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Dart.inject(this);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.main_activity_action_bar_title);

        if(transaction != null) {
            FinishPaymentDialog dialog = FinishPaymentDialog.newInstance(transaction);
            dialog.show(getSupportFragmentManager(), "FinishPaymentDialogTag");
        }
    }

    public void startPaymentClicked(View view) {
        Transaction transaction = new Transaction();
        PaymentManager.getInstance().resetCache();
        Intent intent = EnterAmountActivity.createStartIntent(this, transaction);
        startActivity(intent);
        finish();
    }

    public void illDoItLaterClicked(View view) {
        finish();
    }

}
