package com.mercadopago.dardotest.userinterface.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.mercadopago.dardotest.R;

public class GenericOneButtonDialog extends DialogFragment {
    private static final String MESSAGE_KEY = "message";

    private OnGenericOneButtonDialogClosedListener onGenericOneButtonDialogClosedListener;

    public static GenericOneButtonDialog newInstance(int messageResourceId) {
        GenericOneButtonDialog dialog = new GenericOneButtonDialog();
        Bundle args = new Bundle();
        args.putInt(MESSAGE_KEY, messageResourceId);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setPositiveButton(R.string.ok, onOKPressedListener)
                .create();
        alertDialog.setMessage(getString(getArguments().getInt(MESSAGE_KEY)));
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try { onGenericOneButtonDialogClosedListener = (OnGenericOneButtonDialogClosedListener) context;}
        catch (ClassCastException e) {throw new ClassCastException(context.toString() + " must implement OnGenericOneButtonDialogClosedListener");}
    }

    private DialogInterface.OnClickListener onOKPressedListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            onGenericOneButtonDialogClosedListener.onGenericOneButtonDialogClose(getTag());
        }
    };

    public interface OnGenericOneButtonDialogClosedListener {
        void onGenericOneButtonDialogClose(String tag);
    }
}
