package com.mercadopago.dardotest.userinterface.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.flowmanagers.PaymentManager;
import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.util.ArrayList;
import java.util.List;

public class SelectPaymentMethodViewModel extends AndroidViewModel {

    private Transaction transaction;
    private DisposableRequest ongoingRequest;
    private final PaymentManager paymentManager = PaymentManager.getInstance();
    private final MutableLiveData<List<PaymentMethod>> paymentMethodListObservable = new MutableLiveData<>();
    private final MutableLiveData<String> validateModelResponseObservable = new MutableLiveData<>();

    public MutableLiveData<String> getValidateModelResponseObservable() {
        return validateModelResponseObservable;
    }

    public LiveData<List<PaymentMethod>> getPaymentMethodListObservable() {
        return paymentMethodListObservable;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public SelectPaymentMethodViewModel(@NonNull Application application) {
        super(application);
    }

    public void dispose() {
        if (ongoingRequest != null) {
            ongoingRequest.dispose();
            ongoingRequest = null;
        }
    }

    public void retrieveModel(boolean forceRefresh) {
        ongoingRequest = paymentManager.getCreditCardPaymentMethods(onCreditCardPaymentMethodsCompleted, forceRefresh);
    }

    private OnListRequestCompleted<PaymentMethod> onCreditCardPaymentMethodsCompleted = new OnListRequestCompleted<PaymentMethod>() {
        @Override
        public void onSuccess(ArrayList<PaymentMethod> paymentMethods) {
            ongoingRequest = null;
            paymentMethodListObservable.setValue(paymentMethods);
        }

        @Override
        public void onError(Throwable throwable) {
            ongoingRequest = null;
            paymentMethodListObservable.setValue(null);
        }
    };

    public void selectPaymentMethod(PaymentMethod paymentMethod) {
        transaction.setPaymentMethod(paymentMethod);
        transaction.setCardIssuer(null);
        transaction.setInstallmentItem(null);
    }

    public void validateModel() {
        // TODO Validar cuando paymentMethodListObservable == null
        validateModelResponseObservable.setValue(
                transaction.getPaymentMethod() == null ? getApplication().getString(R.string.error_payment_method) : null
        );
    }
}
