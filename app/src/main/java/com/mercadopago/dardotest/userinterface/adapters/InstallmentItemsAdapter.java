package com.mercadopago.dardotest.userinterface.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.models.InstallmentItem;

import java.util.ArrayList;
import java.util.List;

public class InstallmentItemsAdapter extends RecyclerView.Adapter<InstallmentItemsAdapter.InstallmentItemsViewHolder>{

    private List<InstallmentItem> installmentItems = new ArrayList<>();
    private OnInstallmentItemClickedListener onInstallmentItemClickedListener;
    private int selectedPosition = -1;

    public InstallmentItemsAdapter(OnInstallmentItemClickedListener onInstallmentItemClickedListener) {
        this.onInstallmentItemClickedListener = onInstallmentItemClickedListener;
    }

    public void updateInstallmentItem(List<InstallmentItem> installmentItems) {
        selectedPosition = -1;
        this.installmentItems = installmentItems;
        notifyDataSetChanged();
    }

    public void selectInstallmentItem(int quantities) {
        selectedPosition = -1;
        for(int i=0; i<installmentItems.size(); i++) {
            if(installmentItems.get(i).getQuantities() == quantities) {
                selectedPosition = i;
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public InstallmentItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.installments_item, parent, false);
        return new InstallmentItemsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final InstallmentItemsViewHolder holder, final int position) {
        final InstallmentItem installmentItem = installmentItems.get(position);
        holder.messageTextView.setText(installmentItem.getRecommendedMessage());
        holder.labelTextView.setText(getProperLabel(installmentItem.getLabels()));
        holder.itemView.setBackgroundColor(Color.parseColor(selectedPosition == position ? "#ababae" : "#ffffff"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = holder.getAdapterPosition();
                if(onInstallmentItemClickedListener != null) {
                    onInstallmentItemClickedListener.onInstallmentItemClicked(installmentItem);
                }
                notifyDataSetChanged();
            }
        });
    }

    private String getProperLabel(ArrayList<String> labels) {
        for(String label : labels) {
            if(label.toLowerCase().contains("cft")) {
                return label.replace("|", " - ").replace("_", " ");
            }
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return installmentItems.size();
    }

    public class InstallmentItemsViewHolder extends RecyclerView.ViewHolder {
        public TextView messageTextView;
        public TextView labelTextView;

        public InstallmentItemsViewHolder(View view) {
            super(view);
            messageTextView = view.findViewById(R.id.messageTextView);
            labelTextView = view.findViewById(R.id.labelTextView);
        }
    }

    public interface OnInstallmentItemClickedListener {
        void onInstallmentItemClicked(InstallmentItem installmentItem);
    }
}
