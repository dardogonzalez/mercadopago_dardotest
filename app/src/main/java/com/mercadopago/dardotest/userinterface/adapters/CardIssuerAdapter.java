package com.mercadopago.dardotest.userinterface.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.helpers.ImageDownloader;
import com.mercadopago.dardotest.models.CardIssuer;

import java.util.ArrayList;
import java.util.List;

public class CardIssuerAdapter extends RecyclerView.Adapter<CardIssuerAdapter.CardIssuerViewHolder>{

    private List<CardIssuer> cardIssuers = new ArrayList<>();
    private OnCardIssuerClickedListener onCardIssuerClickedListener;
    private int selectedPosition = -1;

    public CardIssuerAdapter(OnCardIssuerClickedListener onCardIssuerClickedListener) {
        this.onCardIssuerClickedListener = onCardIssuerClickedListener;
    }

    public void updateCardIssuers(List<CardIssuer> cardIssuers) {
        selectedPosition = -1;
        this.cardIssuers = cardIssuers;
        notifyDataSetChanged();
    }

    public void selectCardIssuerById(String id) {
        selectedPosition = -1;
        for(int i=0; i<cardIssuers.size(); i++) {
            if(cardIssuers.get(i).getId().equals(id)) {
                selectedPosition = i;
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public CardIssuerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_issuer_item, parent, false);
        return new CardIssuerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CardIssuerViewHolder holder, final int position) {
        final CardIssuer cardIssuer = cardIssuers.get(position);
        holder.nameTextView.setText(cardIssuer.getName());
        ImageDownloader.getInstance().retrieveImage(cardIssuer.getThumbnail(), holder.avatarImageView, R.mipmap.default_card);
        holder.itemView.setBackgroundColor(Color.parseColor(selectedPosition == position ? "#ababae" : "#ffffff"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = holder.getAdapterPosition();
                if(onCardIssuerClickedListener != null) {
                    onCardIssuerClickedListener.onCardIssuerClicked(cardIssuer);
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardIssuers.size();
    }

    public class CardIssuerViewHolder extends RecyclerView.ViewHolder {
        private ImageView avatarImageView;
        public TextView nameTextView;

        public CardIssuerViewHolder(View view) {
            super(view);
            avatarImageView = view.findViewById(R.id.avatarImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
        }
    }

    public interface OnCardIssuerClickedListener {
        void onCardIssuerClicked(CardIssuer cardIssuer);
    }
}
