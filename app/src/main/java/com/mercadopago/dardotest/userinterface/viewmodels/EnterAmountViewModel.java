package com.mercadopago.dardotest.userinterface.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.helpers.LogHelper;
import com.mercadopago.dardotest.models.Transaction;

import java.math.BigDecimal;

public class EnterAmountViewModel extends AndroidViewModel {

    private Transaction transaction;

    private final MutableLiveData<BigDecimal> amountObservable = new MutableLiveData<>();
    private final MutableLiveData<String> validateModelResponseObservable = new MutableLiveData<>();

    public MutableLiveData<String> getValidateModelResponseObservable() {
        return validateModelResponseObservable;
    }

    public MutableLiveData<BigDecimal> getAmountObservable() {
        return amountObservable;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
        if(transaction.getAmount() != null) {
            amountObservable.setValue(transaction.getAmount());
        }
    }

    public EnterAmountViewModel(@NonNull Application application) {
        super(application);
    }

    public void setAmount(String amount) {
        BigDecimal bigDecimal = BigDecimal.ZERO;
        try {
            if (!amount.isEmpty()) {
                bigDecimal = new BigDecimal(amount).setScale(2);
            }
        } catch (Exception ex) {
            LogHelper.exception(ex);
        }
        transaction.setAmount(bigDecimal);
    }

    public void validateModel() {
        validateModelResponseObservable.setValue(
                (transaction.getAmount() == null  || transaction.getAmount().doubleValue() == 0) ?
                        getApplication().getString(R.string.error_amount) : null
        );
    }
}
