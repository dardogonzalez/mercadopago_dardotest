package com.mercadopago.dardotest.userinterface.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.databinding.ActivityEnterAmountBinding;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.userinterface.viewmodels.EnterAmountViewModel;
import com.mercadopago.dardotest.utils.DecimalDigitsInputFilter;
import com.mercadopago.dardotest.utils.TextWatcherAfterTextChanged;

import java.math.BigDecimal;

public class EnterAmountActivity extends AppCompatActivity {

    private ActivityEnterAmountBinding binding;
    private EnterAmountViewModel viewModel;

    @InjectExtra Transaction transaction;

    public static Intent createStartIntent(Context context, Transaction transaction) {
        return Henson.with(context).gotoEnterAmountActivity().transaction(transaction).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dart.inject(this);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_enter_amount, null, false);
        setContentView(binding.getRoot());

        setupActionBar();
        updateView();
        attachViewModel();
    }

    private void setupActionBar() {
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.enter_amount_activity_action_bar_title);
        binding.toolbar.setNavigationOnClickListener(backNavigationOnClickListener);
    }

    private void updateView() {
        binding.amountTextInputEditText.setFilters(new InputFilter[] {new DecimalDigitsInputFilter()});
        binding.amountTextInputEditText.setOnEditorActionListener(amountTextInputEditTextOnEditorActionListener);
        binding.amountTextInputEditText.addTextChangedListener(amountTextInputEditTextTextChangedListener);
    }

    private void attachViewModel() {
        viewModel = ViewModelProviders.of(this).get(EnterAmountViewModel.class);
        viewModel.setTransaction(transaction);
        viewModel.getAmountObservable().observe(this, amountObserver);
        viewModel.getValidateModelResponseObservable().observe(this, modelValidationResponseObserver);
        binding.setViewModel(viewModel);
    }

    private Observer<BigDecimal> amountObserver = new Observer<BigDecimal>() {
        @Override
        public void onChanged(@Nullable BigDecimal bigDecimal) {
            binding.amountTextInputEditText.setText(String.valueOf(bigDecimal.toString()));
            binding.amountTextInputEditText.selectAll();        }
    };

    private TextView.OnEditorActionListener amountTextInputEditTextOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.validateModel();
                return true;
            }
            return false;
        }
    };

    private TextWatcherAfterTextChanged amountTextInputEditTextTextChangedListener = new TextWatcherAfterTextChanged() {
        @Override
        public void afterTextChanged(Editable s) {
            binding.amountTextInputLayout.setErrorEnabled(false);
            viewModel.setAmount(binding.amountTextInputEditText.getText().toString());
        }
    };

    private Observer<String> modelValidationResponseObserver = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String error) {
            if(error != null) {
                binding.amountTextInputLayout.setErrorEnabled(true);
                binding.amountTextInputLayout.setError(error);
            } else {
                Intent intent = SelectPaymentMethodActivity.createStartIntent(EnterAmountActivity.this, transaction);
                startActivity(intent);
                finish();
            }
        }
    };

    private View.OnClickListener backNavigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = MainActivity.createStartIntent(EnterAmountActivity.this);
        startActivity(intent);
        finish();
    }

}
