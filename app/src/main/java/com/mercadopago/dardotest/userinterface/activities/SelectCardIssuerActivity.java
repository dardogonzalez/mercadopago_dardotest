package com.mercadopago.dardotest.userinterface.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.mercadopago.dardotest.R;
import com.mercadopago.dardotest.databinding.ActivitySelectCardIssuerBinding;
import com.mercadopago.dardotest.models.CardIssuer;
import com.mercadopago.dardotest.models.Transaction;
import com.mercadopago.dardotest.userinterface.adapters.CardIssuerAdapter;
import com.mercadopago.dardotest.userinterface.dialogs.GenericOneButtonDialog;
import com.mercadopago.dardotest.userinterface.viewmodels.SelectCardIssuerViewModel;

import java.util.List;

public class SelectCardIssuerActivity extends AppCompatActivity implements GenericOneButtonDialog.OnGenericOneButtonDialogClosedListener {

    private CardIssuerAdapter cardIssuerAdapter;
    private SelectCardIssuerViewModel viewModel;
    private ActivitySelectCardIssuerBinding binding;

    @InjectExtra Transaction transaction;

    public static Intent createStartIntent(Context context, Transaction transaction) {
        return Henson.with(context).gotoSelectCardIssuerActivity().transaction(transaction).build();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dart.inject(this);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_select_card_issuer, null, false);
        setContentView(binding.getRoot());

        setupActionBar();
        setupView();
        attachViewModel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.dispose();
    }

    private void setupActionBar() {
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.select_card_issuer_activity_action_bar_title);
        binding.toolbar.setNavigationOnClickListener(backNavigationOnClickListener);
    }

    private void setupView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        cardIssuerAdapter = new CardIssuerAdapter(onCardIssuerClickedListener);
        binding.recyclerView.setAdapter(cardIssuerAdapter);
        binding.swipeRefreshLayout.setOnRefreshListener(swipeRefreshLayoutOnRefreshListener);
    }

    private void attachViewModel() {
        viewModel = ViewModelProviders.of(this).get(SelectCardIssuerViewModel.class);
        viewModel.setTransaction(transaction);
        viewModel.getCardIssuerListObservable().observe(this, cardIssuerListObserver);
        viewModel.getValidateModelResponseObservable().observe(this, modelValidationResponseObserver);
        binding.setViewModel(viewModel);
        binding.swipeRefreshLayout.setRefreshing(true);
        viewModel.retrieveModel(false);
    }

    private SwipeRefreshLayout.OnRefreshListener swipeRefreshLayoutOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            viewModel.retrieveModel(true);
        }
    };

    private CardIssuerAdapter.OnCardIssuerClickedListener onCardIssuerClickedListener = new CardIssuerAdapter.OnCardIssuerClickedListener() {
        @Override
        public void onCardIssuerClicked(CardIssuer cardIssuer) {
            viewModel.selectCardIssuer(cardIssuer);
        }
    };

    private Observer<String> modelValidationResponseObserver = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String error) {
            if(error != null) {
                Toast.makeText(SelectCardIssuerActivity.this, error, Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = SelectInstallmentsActivity.createStartIntent(SelectCardIssuerActivity.this, transaction);
                startActivity(intent);
                finish();
            }
        }
    };

    private Observer<List<CardIssuer>> cardIssuerListObserver = new Observer<List<CardIssuer>>() {
        @Override
        public void onChanged(@Nullable List<CardIssuer> cardIssuers) {
            binding.swipeRefreshLayout.setRefreshing(false);
            processCardIssuers(cardIssuers);
        }
    };

    private void processCardIssuers(List<CardIssuer> cardIssuers) {
        if(cardIssuers == null) {
            binding.setModelLoadingError(true);
            return;
        }
        if(cardIssuers.isEmpty()) {
            showCardIssuersNotFound();
            return;
        }
        binding.setModelLoadingError(false);
        cardIssuerAdapter.updateCardIssuers(cardIssuers);
        if (transaction.getCardIssuer() != null) {
            cardIssuerAdapter.selectCardIssuerById(transaction.getCardIssuer().getId());
        }
    }

    private void showCardIssuersNotFound() {
        GenericOneButtonDialog dialog = GenericOneButtonDialog.newInstance(R.string.card_issuers_not_found);
        dialog.show(getSupportFragmentManager(), "GenericOneButtonDialogTag");
    }

    @Override
    public void onGenericOneButtonDialogClose(String tag) {
        onBackPressed();
    }

    private View.OnClickListener backNavigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = MainActivity.createStartIntent(SelectCardIssuerActivity.this);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = SelectPaymentMethodActivity.createStartIntent(this, transaction);
        startActivity(intent);
        finish();
    }

}
