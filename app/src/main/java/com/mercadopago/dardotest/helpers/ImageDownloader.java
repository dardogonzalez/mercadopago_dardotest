package com.mercadopago.dardotest.helpers;

import android.content.Context;
import android.widget.ImageView;

import com.mercadopago.dardotest.MercadoPagoDardoTestApplication;
import com.squareup.picasso.Picasso;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Target;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class ImageDownloader {

    private Picasso picasso;
    private static ImageDownloader imageDownloader;

    private ImageDownloader(Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "picasso-cache");
        Cache cache = new Cache(httpCacheDirectory, 15 * 1024 * 1024);
        OkHttpClient client = new OkHttpClient.Builder().cache(cache).build();
        picasso = new Picasso.Builder(context).downloader(new OkHttp3Downloader(client)).loggingEnabled(true).build();
    }

    public static ImageDownloader getInstance() {
        if(imageDownloader == null) {
            imageDownloader = new ImageDownloader(MercadoPagoDardoTestApplication.getAppContext());
        }
        return imageDownloader;
    }

    public void retrieveImage(String url, ImageView imageView, int placeholderResourceId) {
        if(url != null && imageView != null) {
            picasso.load(url).placeholder(placeholderResourceId).error(placeholderResourceId).into(imageView);
        }
    }

    public void retrieveImage(String url, Target target) {
        if(url != null && target != null) {
            picasso.load(url).into(target);
        }
    }
}
