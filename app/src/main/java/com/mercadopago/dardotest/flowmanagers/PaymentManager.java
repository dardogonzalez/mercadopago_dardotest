package com.mercadopago.dardotest.flowmanagers;

import android.content.Context;

import com.mercadopago.dardotest.MercadoPagoDardoTestApplication;
import com.mercadopago.dardotest.dataproviders.CardIssuerDataProvider;
import com.mercadopago.dardotest.dataproviders.InstallmentDataProvider;
import com.mercadopago.dardotest.dataproviders.PaymentMethodDataProvider;
import com.mercadopago.dardotest.models.CardIssuer;
import com.mercadopago.dardotest.models.Installment;
import com.mercadopago.dardotest.models.InstallmentItem;
import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.math.BigDecimal;

public class PaymentManager {
    private static PaymentManager paymentManager;

    private PaymentMethodDataProvider paymentMethodDataProvider;
    private CardIssuerDataProvider cardIssuerDataProvider;
    private InstallmentDataProvider installmentDataProvider;

    private PaymentManager() {
        paymentMethodDataProvider = PaymentMethodDataProvider.getInstance();
        cardIssuerDataProvider = CardIssuerDataProvider.getInstance();
        installmentDataProvider = InstallmentDataProvider.getInstance();
    }

    public static PaymentManager getInstance() {
        if(paymentManager == null) {
            paymentManager = new PaymentManager();
        }
        return paymentManager;
    }

    public void resetCache() {
        paymentMethodDataProvider.resetCache();
        cardIssuerDataProvider.resetCache();
        installmentDataProvider.resetCache();
    }

    public DisposableRequest getCreditCardPaymentMethods(OnListRequestCompleted<PaymentMethod> onRequestCompleted, boolean forceRefresh) {
        return paymentMethodDataProvider.getCreditCardPaymentMethods(onRequestCompleted, forceRefresh);
    }

    public DisposableRequest getCardIssuers(PaymentMethod paymentMethod, OnListRequestCompleted<CardIssuer> onRequestCompleted, boolean forceRefresh) {
        return cardIssuerDataProvider.getCardIssuers(paymentMethod, onRequestCompleted, forceRefresh);
    }

    public DisposableRequest getInstallments(PaymentMethod paymentMethod, CardIssuer cardIssuer, BigDecimal amount, OnListRequestCompleted<InstallmentItem> onRequestCompleted, boolean forceRefresh) {
        return installmentDataProvider.getInstallments(paymentMethod, cardIssuer, amount, onRequestCompleted, forceRefresh);
    }

    // Este es el método que debería ejecutar el pago. Para esta prueba no se hace nada
    public void executeTransaction() {
    }
}



























