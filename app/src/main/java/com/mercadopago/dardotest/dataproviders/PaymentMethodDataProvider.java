package com.mercadopago.dardotest.dataproviders;

import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.rest.backends.MercadoPagoDardoTestBackend;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListHookUpRequestCompleted;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.util.ArrayList;

public class PaymentMethodDataProvider {

    private static PaymentMethodDataProvider paymentMethodDataProvider;

    private MercadoPagoDardoTestBackend mercadoPagoDardoTestBackend;
    private ArrayList<PaymentMethod> cachedCreditCardPaymentMethods;

    private PaymentMethodDataProvider() {
        mercadoPagoDardoTestBackend = MercadoPagoDardoTestBackend.getInstance();
    }

    public static PaymentMethodDataProvider getInstance() {
        if(paymentMethodDataProvider == null) {
            paymentMethodDataProvider = new PaymentMethodDataProvider();
        }
        return paymentMethodDataProvider;
    }

    public void resetCache() {
        cachedCreditCardPaymentMethods = null;
    }

    public DisposableRequest getCreditCardPaymentMethods(OnListRequestCompleted<PaymentMethod> onRequestCompleted, boolean forceRefresh) {
        if(!forceRefresh && cachedCreditCardPaymentMethods != null) {
            onRequestCompleted.onSuccess(cachedCreditCardPaymentMethods);
            return null;
        }
        return mercadoPagoDardoTestBackend.getPaymentMethods(onRequestCompleted, onHookUpPaymentMethods);
    }

    private OnListHookUpRequestCompleted<PaymentMethod> onHookUpPaymentMethods = new OnListHookUpRequestCompleted<PaymentMethod>() {
        public ArrayList<PaymentMethod> onSuccess(ArrayList<PaymentMethod> paymentMethods) {
            if(!paymentMethods.isEmpty()) {
                cachedCreditCardPaymentMethods = filterCreditCardPaymentMethods(paymentMethods);
            }
            return paymentMethods;
        }
    };

    private ArrayList<PaymentMethod> filterCreditCardPaymentMethods(ArrayList<PaymentMethod> paymentMethods) {
        ArrayList<PaymentMethod> creditCardPaymentMethods = new ArrayList<>();
        for(PaymentMethod paymentMethod : paymentMethods) {
            if(paymentMethod.isCreditCard()) {
                creditCardPaymentMethods.add(paymentMethod);
            }
        }
        return creditCardPaymentMethods;
    }
}
