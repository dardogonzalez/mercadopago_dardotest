package com.mercadopago.dardotest.dataproviders;

import com.mercadopago.dardotest.models.CardIssuer;
import com.mercadopago.dardotest.models.Installment;
import com.mercadopago.dardotest.models.InstallmentItem;
import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.rest.backends.MercadoPagoDardoTestBackend;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListHookUpRequestCompleted;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class InstallmentDataProvider {
    private static InstallmentDataProvider installmentDataProvider;

    private MercadoPagoDardoTestBackend mercadoPagoDardoTestBackend;
    private HashMap<String, HashMap<String, ArrayList<InstallmentItem>>> cachedInstallments = new HashMap<>();

    private InstallmentDataProvider() {
        mercadoPagoDardoTestBackend = MercadoPagoDardoTestBackend.getInstance();
    }

    public static InstallmentDataProvider getInstance() {
        if(installmentDataProvider == null) {
            installmentDataProvider = new InstallmentDataProvider();
        }
        return installmentDataProvider;
    }

    public void resetCache() {
        cachedInstallments = new HashMap<>();
    }

    public DisposableRequest getInstallments(PaymentMethod paymentMethod, CardIssuer cardIssuer, BigDecimal amount, OnListRequestCompleted<InstallmentItem> onRequestCompleted, boolean forceRefresh) {
        if(!forceRefresh && cachedInstallments.containsKey(paymentMethod.getId()) && cachedInstallments.get(paymentMethod.getId()).containsKey(cardIssuer.getId())) {
            onRequestCompleted.onSuccess(cachedInstallments.get(paymentMethod.getId()).get(cardIssuer.getId()));
            return null;
        }
        return mercadoPagoDardoTestBackend.getInstallments(paymentMethod, cardIssuer, amount, new OnInstallmentListRequestCompleted<Installment>(paymentMethod, cardIssuer, onRequestCompleted));
    }

    private class OnInstallmentListRequestCompleted<T> implements OnListRequestCompleted<Installment> {

        private PaymentMethod paymentMethod;
        private CardIssuer cardIssuer;
        private OnListRequestCompleted<InstallmentItem> onRequestCompleted;

        @Override
        public void onSuccess(ArrayList<Installment> installments) {
            if(installments.isEmpty()) {
                onRequestCompleted.onSuccess(new ArrayList<InstallmentItem>());
                return;
            }

            ArrayList<InstallmentItem> installmentItems = installments.get(0).getItems();
            if(!cachedInstallments.containsKey(paymentMethod.getId())) {
                cachedInstallments.put(paymentMethod.getId(), new HashMap<String, ArrayList<InstallmentItem>>());
            }
            if(!cachedInstallments.get(paymentMethod.getId()).containsKey(cardIssuer.getId()) && !installments.isEmpty()) {
                cachedInstallments.get(paymentMethod.getId()).put(cardIssuer.getId(), installmentItems);
            }
            onRequestCompleted.onSuccess(installmentItems);
        }

        @Override
        public void onError(Throwable throwable) {
            onRequestCompleted.onError(throwable);
        }

        public OnInstallmentListRequestCompleted(PaymentMethod paymentMethod, CardIssuer cardIssuer, OnListRequestCompleted<InstallmentItem> onRequestCompleted) {
            this.cardIssuer = cardIssuer;
            this.paymentMethod = paymentMethod;
            this.onRequestCompleted = onRequestCompleted;
        }
    }}
