package com.mercadopago.dardotest.dataproviders;

import com.mercadopago.dardotest.models.CardIssuer;
import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.rest.backends.MercadoPagoDardoTestBackend;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListHookUpRequestCompleted;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;

import java.util.ArrayList;
import java.util.HashMap;

public class CardIssuerDataProvider {
    private static CardIssuerDataProvider cardIssuerDataProvider;

    private MercadoPagoDardoTestBackend mercadoPagoDardoTestBackend;
    private HashMap<String, ArrayList<CardIssuer>> cachedCardIssuers = new HashMap<>();

    private CardIssuerDataProvider() {
        mercadoPagoDardoTestBackend = MercadoPagoDardoTestBackend.getInstance();
    }

    public static CardIssuerDataProvider getInstance() {
        if(cardIssuerDataProvider == null) {
            cardIssuerDataProvider = new CardIssuerDataProvider();
        }
        return cardIssuerDataProvider;
    }

    public void resetCache() {
        cachedCardIssuers = new HashMap<>();
    }

    public DisposableRequest getCardIssuers(PaymentMethod paymentMethod, OnListRequestCompleted<CardIssuer> onRequestCompleted, boolean forceRefresh) {
        if(!forceRefresh && cachedCardIssuers.containsKey(paymentMethod.getId())) {
            onRequestCompleted.onSuccess(cachedCardIssuers.get(paymentMethod.getId()));
            return null;
        }
        return mercadoPagoDardoTestBackend.getCardIssuers(paymentMethod, onRequestCompleted, new ListHookUpCardIssuers<CardIssuer>(paymentMethod));
    }

    private class ListHookUpCardIssuers<T> extends OnListHookUpRequestCompleted<CardIssuer> {

        private PaymentMethod paymentMethod;

        public ListHookUpCardIssuers(PaymentMethod paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        @Override
        public ArrayList<CardIssuer> onSuccess(ArrayList<CardIssuer> cardIssuers) {
            if(!cachedCardIssuers.containsKey(paymentMethod.getId()) && !cardIssuers.isEmpty()) {
                cachedCardIssuers.put(paymentMethod.getId(), cardIssuers);
            }
            return cardIssuers;
        }
    }

}
