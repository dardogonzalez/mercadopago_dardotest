package com.mercadopago.dardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Installment implements Parcelable {

    private String paymentMethodId;
    private String cardIssuerId;
    private ArrayList<InstallmentItem> items = new ArrayList<>();

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getCardIssuerId() {
        return cardIssuerId;
    }

    public void setCardIssuerId(String cardIssuerId) {
        this.cardIssuerId = cardIssuerId;
    }

    public ArrayList<InstallmentItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<InstallmentItem> items) {
        this.items = items;
    }

    public Installment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.paymentMethodId);
        dest.writeString(this.cardIssuerId);
        dest.writeTypedList(this.items);
    }

    protected Installment(Parcel in) {
        this.paymentMethodId = in.readString();
        this.cardIssuerId = in.readString();
        this.items = in.createTypedArrayList(InstallmentItem.CREATOR);
    }

    public static final Creator<Installment> CREATOR = new Creator<Installment>() {
        @Override
        public Installment createFromParcel(Parcel source) {
            return new Installment(source);
        }

        @Override
        public Installment[] newArray(int size) {
            return new Installment[size];
        }
    };
}