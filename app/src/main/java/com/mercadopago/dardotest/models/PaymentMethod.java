package com.mercadopago.dardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentMethod implements Parcelable {
    public enum PaymentTypes {
        CreditCard("credit_card"),
        Ticket("ticket"),
        DebitCard("debit_card");

        private String type;

        PaymentTypes(String type) {
            this.type = type;
        }

        public String getValue() {
            return type;
        }
    }

    private String id;
    private String name;
    private String paymentTypeId;
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isCreditCard() {
        return PaymentTypes.CreditCard.getValue().equals(paymentTypeId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.paymentTypeId);
        dest.writeString(this.thumbnail);
    }

    public PaymentMethod() {
    }

    protected PaymentMethod(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.paymentTypeId = in.readString();
        this.thumbnail = in.readString();
    }

    public static final Parcelable.Creator<PaymentMethod> CREATOR = new Parcelable.Creator<PaymentMethod>() {
        @Override
        public PaymentMethod createFromParcel(Parcel source) {
            return new PaymentMethod(source);
        }

        @Override
        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };
}
