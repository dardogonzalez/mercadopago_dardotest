package com.mercadopago.dardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class InstallmentItem implements Parcelable {
    private int quantities;
    private String recommendedMessage;
    private ArrayList<String> labels = new ArrayList<>();

    public int getQuantities() {
        return quantities;
    }

    public void setQuantities(int quantities) {
        this.quantities = quantities;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<String> labels) {
        this.labels = labels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.quantities);
        dest.writeString(this.recommendedMessage);
        dest.writeStringList(this.labels);
    }

    public InstallmentItem() {
    }

    protected InstallmentItem(Parcel in) {
        this.quantities = in.readInt();
        this.recommendedMessage = in.readString();
        this.labels = in.createStringArrayList();
    }

    public static final Creator<InstallmentItem> CREATOR = new Creator<InstallmentItem>() {
        @Override
        public InstallmentItem createFromParcel(Parcel source) {
            return new InstallmentItem(source);
        }

        @Override
        public InstallmentItem[] newArray(int size) {
            return new InstallmentItem[size];
        }
    };
}
