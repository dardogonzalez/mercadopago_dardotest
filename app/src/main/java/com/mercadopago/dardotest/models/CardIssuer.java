package com.mercadopago.dardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

public class CardIssuer implements Parcelable {

    private String id;
    private String name;
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.thumbnail);
    }

    public CardIssuer() {
    }

    protected CardIssuer(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.thumbnail = in.readString();
    }

    public static final Parcelable.Creator<CardIssuer> CREATOR = new Parcelable.Creator<CardIssuer>() {
        @Override
        public CardIssuer createFromParcel(Parcel source) {
            return new CardIssuer(source);
        }

        @Override
        public CardIssuer[] newArray(int size) {
            return new CardIssuer[size];
        }
    };
}
