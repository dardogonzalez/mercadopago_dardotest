package com.mercadopago.dardotest.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class Transaction implements Parcelable {

    private BigDecimal amount;
    public PaymentMethod paymentMethod;
    public CardIssuer cardIssuer;
    public InstallmentItem installmentItem;

    public InstallmentItem getInstallmentItem() {
        return installmentItem;
    }

    public void setInstallmentItem(InstallmentItem installmentItem) {
        this.installmentItem = installmentItem;
    }


    public CardIssuer getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(CardIssuer cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Transaction() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.amount);
        dest.writeParcelable(this.paymentMethod, flags);
        dest.writeParcelable(this.cardIssuer, flags);
        dest.writeParcelable(this.installmentItem, flags);
    }

    protected Transaction(Parcel in) {
        this.amount = (BigDecimal) in.readSerializable();
        this.paymentMethod = in.readParcelable(PaymentMethod.class.getClassLoader());
        this.cardIssuer = in.readParcelable(CardIssuer.class.getClassLoader());
        this.installmentItem = in.readParcelable(InstallmentItem.class.getClassLoader());
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel source) {
            return new Transaction(source);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };
}
