package com.mercadopago.dardotest.rest.backends;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Esta clase se debe implementar para cada backend
 * con el que la app esté interactuando
 */
public abstract class BaseBackend<T> {

    protected T api;

    abstract String getBackendDomain();
    abstract Class<T> apiClass();

    public BaseBackend() {
        api = configureRetrofit();
    }

    private T configureRetrofit() {

        /**
         * Agregar lógica que cuando se compile como production release para
         * subir al playstore el logging sea NONE.
         */
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return (T) new Retrofit.Builder()
                .baseUrl(getBackendDomain())
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(apiClass());
    }
}
