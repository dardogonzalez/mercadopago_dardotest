package com.mercadopago.dardotest.rest.support;

import java.util.ArrayList;

public abstract class OnListHookUpRequestCompleted<T> {
    public abstract ArrayList<T> onSuccess(ArrayList<T> t);
    void onError(Throwable throwable) {
    };
}
