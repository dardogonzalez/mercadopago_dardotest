package com.mercadopago.dardotest.rest.backends;

import com.mercadopago.dardotest.models.CardIssuer;
import com.mercadopago.dardotest.models.Installment;
import com.mercadopago.dardotest.models.PaymentMethod;
import com.mercadopago.dardotest.rest.support.DisposableRequest;
import com.mercadopago.dardotest.rest.support.OnListHookUpRequestCompleted;
import com.mercadopago.dardotest.rest.support.OnListRequestCompleted;
import com.mercadopago.dardotest.rest.support.RequestWrapperForList;

import java.math.BigDecimal;

public class MercadoPagoDardoTestBackend extends BaseBackend<MercadoPagoDardoTestBackendAPIs>{
    
    private static MercadoPagoDardoTestBackend mercadoPagoDardoTestBackend;
    private static final String publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88";

    private MercadoPagoDardoTestBackend() {
        super();
    }

    public static MercadoPagoDardoTestBackend getInstance() {
        if(mercadoPagoDardoTestBackend == null) {
            mercadoPagoDardoTestBackend = new MercadoPagoDardoTestBackend();
        }
        return mercadoPagoDardoTestBackend;
    }

    @Override
    String getBackendDomain() {
        /***
         * Aquí se debería configurar la url del backend dependiendo
         * del flavor que se este usando en esta compilacion
         * segun los definidos en el archivo de gradle.
         * Por ejemplo
         *      environment development - "https://public-apis.dev.mercadopago.com/"
         *      environment production - "https://public-apis.mercadopago.com/"
         * Para este test lo dejo hardcodeado con la url de prueba
         */
        return "https://api.mercadopago.com/v1/";
    }

    @Override
    Class<MercadoPagoDardoTestBackendAPIs> apiClass() {
        return MercadoPagoDardoTestBackendAPIs.class;
    }

    public DisposableRequest getPaymentMethods(OnListRequestCompleted<PaymentMethod> onRequestCompleted, OnListHookUpRequestCompleted<PaymentMethod>... onHookUpRequestCompleted) {
        RequestWrapperForList requestWrapper = new RequestWrapperForList<>(api.PaymentMethods(publicKey), onRequestCompleted, onHookUpRequestCompleted.length == 0 ? null : onHookUpRequestCompleted[0]);
        requestWrapper.executeAsync();
        return requestWrapper;
    }

    public DisposableRequest getCardIssuers(PaymentMethod paymentMethod, OnListRequestCompleted<CardIssuer> onRequestCompleted, OnListHookUpRequestCompleted<CardIssuer>... onHookUpRequestCompleted) {
        RequestWrapperForList requestWrapper = new RequestWrapperForList<>(api.getCardIssuers(publicKey, paymentMethod.getId()), onRequestCompleted, onHookUpRequestCompleted.length == 0 ? null : onHookUpRequestCompleted[0]);
        requestWrapper.executeAsync();
        return requestWrapper;
    }

    public DisposableRequest getInstallments(PaymentMethod paymentMethod, CardIssuer cardIssuer, BigDecimal amount, OnListRequestCompleted<Installment> onRequestCompleted, OnListHookUpRequestCompleted<Installment>... onHookUpRequestCompleted) {
        RequestWrapperForList requestWrapper = new RequestWrapperForList<>(api.getInstallments(publicKey, amount.toString(), paymentMethod.getId(), cardIssuer.getId()), onRequestCompleted, onHookUpRequestCompleted.length == 0 ? null : onHookUpRequestCompleted[0]);
        requestWrapper.executeAsync();
        return requestWrapper;
    }
}
