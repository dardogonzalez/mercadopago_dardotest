package com.mercadopago.dardotest.rest.support;

/**
 * Interface que permite que desde un activity se pueda
 * cancelar una llamada sin exponer todos los métodos
 * que implementa la lógica para hacer los requests
 */
public interface DisposableRequest {
    void dispose();
}
