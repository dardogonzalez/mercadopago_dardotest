package com.mercadopago.dardotest.rest.backendmodels;

import com.google.gson.annotations.SerializedName;
import com.mercadopago.dardotest.models.CardIssuer;

public class JsonBackendCardIssuer extends JsonBackendModelOf<CardIssuer> {

    @SerializedName("id") private String id;
    @SerializedName("name") private String name;
    @SerializedName("thumbnail") private String thumbnail;

    @Override
    public CardIssuer getLocalModel() {
        CardIssuer cardIssuer = new CardIssuer();
        cardIssuer.setId(id);
        cardIssuer.setName(name);
        cardIssuer.setThumbnail(thumbnail);
        return cardIssuer;
    }
}
