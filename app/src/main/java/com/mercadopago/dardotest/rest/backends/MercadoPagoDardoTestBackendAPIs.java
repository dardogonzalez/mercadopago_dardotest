package com.mercadopago.dardotest.rest.backends;

import com.mercadopago.dardotest.rest.backendmodels.JsonBackendCardIssuer;
import com.mercadopago.dardotest.rest.backendmodels.JsonBackendInstallment;
import com.mercadopago.dardotest.rest.backendmodels.JsonBackendPaymentMethod;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MercadoPagoDardoTestBackendAPIs {

    @GET("payment_methods")
    Call<List<JsonBackendPaymentMethod>> PaymentMethods(
            @Query("public_key") String publicKey
    );

    @GET("payment_methods/card_issuers")
    Call<List<JsonBackendCardIssuer>> getCardIssuers(
            @Query("public_key") String publicKey,
            @Query("payment_method_id") String paymentMethodId
    );

    @GET("payment_methods/installments")
    Call<List<JsonBackendInstallment>> getInstallments(
            @Query("public_key") String publicKey,
            @Query("amount") String amount,
            @Query("payment_method_id") String paymentMethodId,
            @Query("issuer.id") String cardIssuerId
    );
}
