package com.mercadopago.dardotest.rest.support;

import com.mercadopago.dardotest.rest.backendmodels.JsonBackendModelOf;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestWrapperForList<T, S extends JsonBackendModelOf<T>> implements Callback<List<S>>, DisposableRequest {

    private final Call<List<S>> call;
    private final OnListRequestCompleted<T> onRequestCompleted;
    private final OnListHookUpRequestCompleted<T> onListHookUpRequestCompleted;

    // Si se pidió la cancelacion no se ejecuta el callback, independientemente
    // de si la llamada al backend fue efectivamente cancelada o no
    private boolean cancelSignalled;

    public RequestWrapperForList(Call<List<S>> call, OnListRequestCompleted<T> onRequestCompleted) {
        this(call, onRequestCompleted, null);
    }

    public RequestWrapperForList(Call<List<S>> call, OnListRequestCompleted<T> onRequestCompleted, OnListHookUpRequestCompleted<T> onListHookUpRequestCompleted) {
        this.call = call;
        this.onRequestCompleted = onRequestCompleted;
        this.onListHookUpRequestCompleted = onListHookUpRequestCompleted;
    }

    @Override
    public void dispose() {
        cancelSignalled = true;
        call.cancel();
    }

    public void executeAsync() {
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<List<S>> call, Response<List<S>> response) {
        if (cancelSignalled) {
            return;
        }
        if (response.isSuccessful()) {
            processSuccessfulResponse(response.body());
        } else {
            /**
             * Aquí se deberían administrar los posibles errores de red.
             * Para esta prueba solamente copio el error code.
             */
            onRequestCompleted.onError(new Throwable(String.valueOf(response.code())));
        }
    }

    private void processSuccessfulResponse(List<S> jsonBackendModelList) {
        ArrayList<T> localModelList = new ArrayList<>();
        for(JsonBackendModelOf<T> jsonBackendModel : jsonBackendModelList) {
            localModelList.add(jsonBackendModel.getLocalModel());
        }
        if(onListHookUpRequestCompleted != null) {
            localModelList = onListHookUpRequestCompleted.onSuccess(localModelList);
        }
        onRequestCompleted.onSuccess(localModelList);
    }

    @Override
    public void onFailure(Call<List<S>> call, Throwable t) {
        if(!cancelSignalled) {
            onRequestCompleted.onError(t);
        }
    }
}
