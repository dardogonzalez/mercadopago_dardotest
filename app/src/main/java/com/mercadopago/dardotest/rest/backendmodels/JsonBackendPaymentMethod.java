package com.mercadopago.dardotest.rest.backendmodels;

import com.google.gson.annotations.SerializedName;
import com.mercadopago.dardotest.models.PaymentMethod;

public class JsonBackendPaymentMethod extends JsonBackendModelOf<PaymentMethod> {

    @SerializedName("id") private String id;
    @SerializedName("name") private String name;
    @SerializedName("payment_type_id") private String paymentTypeId;
    @SerializedName("thumbnail") private String thumbnail;

    @Override
    public PaymentMethod getLocalModel() {
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(id);
        paymentMethod.setName(name);
        paymentMethod.setPaymentTypeId(paymentTypeId);
        paymentMethod.setThumbnail(thumbnail);
        return paymentMethod;
    }
}
