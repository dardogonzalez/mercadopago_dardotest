package com.mercadopago.dardotest.rest.backendmodels;

import com.google.gson.annotations.SerializedName;
import com.mercadopago.dardotest.models.Installment;
import com.mercadopago.dardotest.models.InstallmentItem;

import java.util.ArrayList;
import java.util.List;

public class JsonBackendInstallment extends JsonBackendModelOf<Installment> {

    @SerializedName("payment_method_id") private String paymentMethodId;
    @SerializedName("issuer") private JsonBackendIssuer jsonBackendIssuer;
    @SerializedName("payer_costs") private List<JsonBackendPayerCost> jsonBackendPayerCosts;

    @Override
    public Installment getLocalModel() {
        Installment installment = new Installment();
        installment.setPaymentMethodId(paymentMethodId);
        if(jsonBackendIssuer != null) {
            installment.setCardIssuerId(jsonBackendIssuer.id);
        }
        if(jsonBackendPayerCosts != null) {
            for(JsonBackendPayerCost jsonBackendPayerCost : jsonBackendPayerCosts) {
                InstallmentItem installmentItem = new InstallmentItem();
                installmentItem.setQuantities(jsonBackendPayerCost.installments);
                installmentItem.setRecommendedMessage(jsonBackendPayerCost.recommendedMessage);
                installmentItem.setLabels((ArrayList<String>) jsonBackendPayerCost.labels);
                installment.getItems().add(installmentItem);
            }
        }
        return installment;
    }

    private class JsonBackendIssuer {
        @SerializedName("id") private String id;
    }

    private class JsonBackendPayerCost {
        @SerializedName("installments") private int installments;
        @SerializedName("recommended_message") private String recommendedMessage;
        @SerializedName("labels") private List<String> labels;
    }
}
