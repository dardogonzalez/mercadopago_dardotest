package com.mercadopago.dardotest.rest.support;

public interface OnItemRequestCompleted<T> {
    void onSuccess(T t);
    void onError(Throwable throwable);
}
