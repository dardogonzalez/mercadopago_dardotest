package com.mercadopago.dardotest.rest.support;

public abstract class OnHookUpRequestCompleted<T> {
    abstract T onSuccess(T t);
    void onError(Throwable throwable) {
    };
}
