package com.mercadopago.dardotest.rest.support;

import java.util.ArrayList;

public interface OnListRequestCompleted<T> {
    void onSuccess(ArrayList<T> t);
    void onError(Throwable throwable);
}

