package com.mercadopago.dardotest.rest.support;

import com.mercadopago.dardotest.rest.backendmodels.JsonBackendModelOf;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestWrapperForItem<T, S extends JsonBackendModelOf<T>> implements Callback<S>, DisposableRequest {

    private final Call<S> call;
    private final OnItemRequestCompleted<T> onRequestCompleted;
    private final OnHookUpRequestCompleted<T> onHookUpRequestCompleted;

    // Si se pidió la cancelacion no se ejecuta el callback, independientemente
    // de si la llamada al backend fue efectivamente cancelada o no
    private boolean cancelSignalled;

    public RequestWrapperForItem(Call<S> call, OnItemRequestCompleted<T> onRequestCompleted) {
        this(call, onRequestCompleted, null);
    }

    public RequestWrapperForItem(Call<S> call, OnItemRequestCompleted<T> onRequestCompleted, OnHookUpRequestCompleted<T> onHookUpRequestCompleted) {
        this.call = call;
        this.onRequestCompleted = onRequestCompleted;
        this.onHookUpRequestCompleted = onHookUpRequestCompleted;
    }

    @Override
    public void dispose() {
        cancelSignalled = true;
        call.cancel();
    }

    public void executeAsync() {
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<S> call, Response<S> response) {
        if (cancelSignalled) {
            return;
        }
        if (response.isSuccessful()) {
            T localModel = response.body().getLocalModel();
            if(onHookUpRequestCompleted != null) {
                localModel = onHookUpRequestCompleted.onSuccess(localModel);
            }
            onRequestCompleted.onSuccess(localModel);
        } else {
            /**
             * Aquí se deberían administrar los posibles errores de red.
             * Para esta prueba solamente copio el error code.
             */
            onRequestCompleted.onError(new Throwable(String.valueOf(response.code())));
        }
    }

    @Override
    public void onFailure(Call<S> call, Throwable t) {
        if(!cancelSignalled) {
            onRequestCompleted.onError(t);
        }
    }
}
