package com.mercadopago.dardotest.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DecimalDigitsInputFilter implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        Pattern pattern = Pattern.compile("[0-9]*+((\\.[0-9]{0,1})?)||(\\.)?");
        Matcher matcher=pattern.matcher(dest);
        return !matcher.matches()? "" : null;
    }
}